<?php

namespace BackendBundle\Controller\GerantStation;


use BackendBundle\Form\ProduitType;
use BackendBundle\Form\StationType;
use Knp\Bundle\SnappyBundle\Snappy\Response\PdfResponse;
use Knp\Snappy\Pdf;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Dompdf\Dompdf;
use Dompdf\Options;
use WebBundle\Entity\Produits;
use WebBundle\Entity\Station;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
/**
 * @Route("/produitgerant")
 */
class ProduitController extends Controller
{
    /**
     *
     * @Route("/", name="produit_gerant_index", defaults={"page": 1})
     * @Route("/page/{page}", requirements={"page": "[1-9]\d*"}, name="produit_gerant_index_paginated")
     * @Method("GET")
     */
    public function index(Request $request,$page)
    {

        $em = $this->getDoctrine()->getManager();
        $search=$request->get('search',"");
        $categorie=$request->get('categorie',"");
        $souscategorie=$request->get('souscategorie',"");

        $categories =$em->getRepository('WebBundle:Categories')->getCategorieByPays($this->getUser()->getPays(),"");
        $souscategories =$em->getRepository('WebBundle:SousCategories')->getSousCategorieByPays($this->getUser()->getPays(),"","");


        $produits =$em->getRepository('WebBundle:Produits')->getProduitByStation($this->getUser()->getStation(),$search,$categorie,$souscategorie);
        $paginator = $this->get('knp_paginator');
        $produits_paginator = $paginator->paginate(
            $produits, $page, 10
        //Produits::NUM_ITEMS
        );
        $produits_paginator->setUsedRoute('produit_gerant_index_paginated');

        return $this->render('@Backend/gerant/produit/index.html.twig', [
            'produits' => $produits_paginator,
            'search' => $search,
            'categorie' => $categorie,
            'souscategorie' => $souscategorie,
            'categories' => $categories,
            'souscategories' => $souscategories,
        ]);
    }
    /**
     * @Route("/get_sous_categorie_from_categorie", name="get_sous_categorie_from_categorie_gerant", methods={"GET"})
     */
    public function getSousCategorieFromCategorie(Request $request)
    {
        // Get Entity manager and repository
        $em = $this->getDoctrine()->getManager();
        $sousCaregorieRepository = $em->getRepository("WebBundle:SousCategories");

        // Search the neighborhoods that belongs to the city with the given id as GET parameter "cityid"
        $sousCategories = $sousCaregorieRepository->createQueryBuilder("sc")
            ->where("sc.categorie = :categorieid")
            ->setParameter("categorieid", $request->query->get("categorieid"))
            ->getQuery()
            ->getResult();

        // Serialize into an array the data that we need, in this case only name and id
        // Note: you can use a serializer as well, for explanation purposes, we'll do it manually
        $responseArray = array();
        foreach($sousCategories as $souscategorie){
            $responseArray[] = array(
                "id" => $souscategorie->getId(),
                "nom" => $souscategorie->getNom()
            );
        }

        return new JsonResponse($responseArray);

    }
    /**
     * @Route("/new", name="new_produit_gerant", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $produit = new Produits();
        $formOptions = array('pays' => $this->getUser()->getPays());

        $form = $this->createForm(ProduitType::class, $produit,$formOptions);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /*  $image=  $form->get('imagefile')->getData();
                        if ($image) {
                            $originalFilename = pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME);
                            $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
                            $newFilename = $safeFilename.'-'.uniqid().'.'.$image->guessExtension();

                            try {
                                $image->move(
                                    $this->getParameter('image_directory'),
                                    $newFilename
                                );
                            } catch (FileException $e) {
                            }
                            $produit->setImage($newFilename);
                        }
                        */
            $entityManager = $this->getDoctrine()->getManager();
            $produit->setStation($this->getUser()->getStation());
            $produit->setPays($this->getUser()->getPays());
            $produit->setCode(uniqid());
            $produit->setDescription('');
            $entityManager->persist($produit);
            $entityManager->flush();

            return $this->redirectToRoute('produit_gerant_index');
        }

        return $this->render('@Backend/gerant/produit/new.html.twig', [
            'produit' => $produit,
            'form' => $form->createView(),
        ]);
    }


    /**
     * @Route("/{id}/edit", name="produit_gerant_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Produits $produit): Response
    {
        $formOptions = array('pays' => $this->getUser()->getPays());

        $form = $this->createForm(ProduitType::class, $produit,$formOptions);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('produit_gerant_index');
        }

        return $this->render('@Backend/gerant/produit/edit.html.twig', [
            'produit' => $produit,
            'form' => $form->createView(),
        ]);
    }

    /**
     * disable station.
     *
     * @Route("/{id}/disable", name="produit_gerant_disable", methods={"GET"})
     */
    public function disableAction(Produits $produit)
    {
        $em = $this->getDoctrine()->getManager();
        $onestation =$em->getRepository('WebBundle:Produits')->find($produit->getId());
        $onestation->setIsActive(false);
        $em->persist($onestation);
        $em->flush();
        return $this->redirectToRoute('produit_gerant_index');
    }

    /**
     * @Route("/Liste_des_produits_gerant", name="ExportProduit_gerant" ,defaults={"_format"="xls","_filename"="Liste_des_stations"}, requirements={"_format"="csv|xls|xlsx"})
     * @Template("@Backend/excel/StationExcel.xlsx.twig")
     */
    public function ExportModeleAction($_filename, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $search=$request->get('search',"");
        $categorie=$request->get('categorie',"");
        $souscategorie=$request->get('souscategorie',"");
        $produits =$em->getRepository('WebBundle:Produits')->getProduitByStation($this->getUser()->getStation(),$search,$categorie,$souscategorie);
        return $this->render('@Backend/excel/ProduitExcel.xlsx.twig', array(
            'Modeles' => $produits,
        ));
    }
}
