<?php

namespace BackendBundle\Controller\GestionnairePays;


use BackendBundle\Form\GerantType;
use BackendBundle\Form\PaysType;
use BackendBundle\Utils\ExportExcelStyle;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;
use WebBundle\Entity\Pays;
use WebBundle\Entity\Utilisateurs;
use WebBundle\Repository\PaysRepository;
use Symfony\Component\Intl\Intl;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * @Route("/client")
 */
class ClientController extends Controller
{

    /**
     *
     * @Route("/", name="client_index", defaults={"page": 1})
     * @Route("/page/{page}", requirements={"page": "[1-9]\d*"}, name="client_index_paginated")
     * @Method("GET")
     */
    public function index(Request $request,$page)
    {

        $em = $this->getDoctrine()->getManager();
        $email=$request->get('email',"");
        $clients =$em->getRepository('WebBundle:Utilisateurs')->getClientByPays($this->getUser()->getPays()->getId(),$email);
        $paginator = $this->get('knp_paginator');
        $clients_paginator = $paginator->paginate(
            $clients, $page, 10
        //Produits::NUM_ITEMS
        );
        $clients_paginator->setUsedRoute('client_index_paginated');
        return $this->render('@Backend/gestionnaire/client/index.html.twig', [
            'email' => $email,
            'clients' => $clients_paginator,
        ]);

    }


    /**
     * @Route("/{id}/detail", name="client_detail", methods={"GET","POST"})
     */
    public function edit(Request $request, Utilisateurs $client)
    {



      /*  return $this->render('@Backend/gestionnaire/gerant/edit.html.twig', [
            'gerant' => $gerant,
            'form' => $form->createView(),
        ]);*/
    }


    /**
     * @Route("/Liste_des_clients", name="ExportClient" ,defaults={"_format"="xls","_filename"="Liste_des_clients"}, requirements={"_format"="csv|xls|xlsx"})
     * @Template("@Backend/excel/ClientExcel.xlsx.twig")
     */
    public function ExportModeleAction($_filename, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $email=$request->get('email',"");
        $gerants =$em->getRepository('WebBundle:Utilisateurs')->getClientByPays($this->getUser()->getPays()->getId(),$email);
        return $this->render('@Backend/excel/ClientExcel.xlsx.twig', array(
            'Modeles' => $gerants,
        ));
    }
}
