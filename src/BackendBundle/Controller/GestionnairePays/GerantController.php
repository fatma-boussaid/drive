<?php

namespace BackendBundle\Controller\GestionnairePays;


use BackendBundle\Form\GerantType;
use BackendBundle\Form\PaysType;
use BackendBundle\Utils\ExportExcelStyle;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;
use WebBundle\Entity\Pays;
use WebBundle\Entity\Utilisateurs;
use WebBundle\Repository\PaysRepository;
use Symfony\Component\Intl\Intl;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * @Route("/gerant")
 */
class GerantController extends Controller
{

    /**
     *
     * @Route("/", name="gerant_index", defaults={"page": 1})
     * @Route("/page/{page}", requirements={"page": "[1-9]\d*"}, name="gerant_index_paginated")
     * @Method("GET")
     */
    public function index(Request $request,$page)
    {
        $em = $this->getDoctrine()->getManager();
        $email=$request->get('email',"");

        $gerants =$em->getRepository('WebBundle:Utilisateurs')->getGerantByPays($this->getUser()->getPays()->getId(),$email);
        $paginator = $this->get('knp_paginator');
        $gerants_paginator = $paginator->paginate(
            $gerants, $page, 10
        //Produits::NUM_ITEMS
        );
        $gerants_paginator->setUsedRoute('gerant_index_paginated');
        return $this->render('@Backend/gestionnaire/gerant/index.html.twig', [
            'email' => $email,
            'gerants' => $gerants_paginator,
        ]);

    }

    /**
     * @Route("/new", name="gerant_new", methods={"GET","POST"})
     */
    public function new(Request $request)
    {
        $formOptions = array('pays' => $this->getUser()->getPays());
        $gerant = new Utilisateurs();
        $form = $this->createForm(GerantType::class, $gerant,$formOptions);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();

            $gerant->setEnabled(true);
            $gerant->setRoles(array('ROLE_GERANT'));
            $gerant->setPays($this->getUser()->getPays());
            $entityManager->persist($gerant);
            $entityManager->flush();

            return $this->redirectToRoute('gerant_index');
        }

        return $this->render('@Backend/gestionnaire/gerant/new.html.twig', [
            'gerant' => $gerant,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="gerant_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Utilisateurs $gerant)
    {
        $formOptions = array('pays' => $this->getUser()->getPays());
        $form = $this->createForm(GerantType::class, $gerant,$formOptions);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $gerant->setEnabled(true);
            $gerant->setRoles(array('ROLE_GERANT'));
            $gerant->setPays($this->getUser()->getPays());
            return $this->redirectToRoute('gerant_index');
        }

        return $this->render('@Backend/gestionnaire/gerant/edit.html.twig', [
            'gerant' => $gerant,
            'form' => $form->createView(),
        ]);
    }

    /**
     * disable user.
     *
     * @Route("/{id}/disable", name="gerant_disable", methods={"GET"})
     */
    public function disableAction(Utilisateurs $user)
    {
        $em = $this->getDoctrine()->getManager();
        $oneUser=$em->getRepository('WebBundle:Utilisateurs')->find($user->getId());
        $oneUser->setEnabled(false);
        $em->persist($oneUser);
        $em->flush();
        return $this->redirectToRoute('gerant_index');
    }


    /**
     * @Route("/Liste_des_gerants", name="ExportGerant" ,defaults={"_format"="xls","_filename"="Liste_des_gerants"}, requirements={"_format"="csv|xls|xlsx"})
     * @Template("@Backend/excel/GerantExcel.xlsx.twig")
     */
    public function ExportModeleAction($_filename, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $email=$request->get('email',"");
        $gerants =$em->getRepository('WebBundle:Utilisateurs')->getGerantByPays($this->getUser()->getPays()->getId(),$email);
        return $this->render('@Backend/excel/GerantExcel.xlsx.twig', array(
            'Modeles' => $gerants,
        ));
    }
}
