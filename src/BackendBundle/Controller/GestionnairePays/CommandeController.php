<?php

namespace BackendBundle\Controller\GestionnairePays;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use WebBundle\Entity\Commande_Produit;
use WebBundle\Entity\Produits;
use WebBundle\Form\ProduitsType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
/**
 * Produits controller.
 *
 * @Route("/commande")
 */
class CommandeController extends Controller
{
    /**
     * Lists all Produits entities.
     *
     * @Route("/", name="commande_index", defaults={"page": 1})
     * @Route("/page/{page}", requirements={"page": "[1-9]\d*"}, name="commande_index_paginated")
     * @Method("GET")
     */
    public function indexAction(Request $request,$page)
    {
        $em = $this->getDoctrine()->getManager();
        $reference=$request->get('reference','');
        $statut=$request->get('statut','');
        $client=$request->get('client','');

        $query = $em->getRepository('WebBundle:Commande_Produit')->GetCommandeByPays($this->getuser()->getPays(),$reference,$statut,$client);

        $paginator = $this->get('knp_paginator');

        $commandes = $paginator->paginate(
            $query, $page, 10
        );
        $commandes->setUsedRoute('commande_index_paginated');
        return $this->render('@Backend/gestionnaire/commande/index.html.twig', array(
            'commandes' => $commandes,
            'reference' =>$reference,
            'statut'=> $statut,
            'client'=> $client

        ));
    }


    /**
     * Finds and displays a Produits entity.
     *
     * @Route("/show/{id}", name="commande_show", defaults={"page": 1})
     * @Route("/page/{page}", requirements={"page": "[1-9]\d*"}, name="commandes_show_paginated")
     * @Method("GET")
     */
    public function showAction( $id,$page)
    { 
        $em = $this->getDoctrine()->getManager();
        $commande = $em->getRepository('WebBundle:Commandes')->find($id);
        $query= $em->getRepository('WebBundle:Commande_Produit')->GetProduitByCommande($commande);
        $paginator = $this->get('knp_paginator');
        $commande_produit = $paginator->paginate(
            $query, $page, 10
        //Produits::NUM_ITEMS
        );
        return $this->render('@Backend/gestionnaire/commande/show.html.twig', array(
           'commande' => $commande,
          'commande_produit' => $commande_produit,
            ));
    }

    /**
     * Displays a form to edit an existing Produits entity.
     *
     * @Route("/edit/{id}/{statut}", name="commnde_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction($id,$statut)
    {

        $em = $this->getDoctrine()->getManager();
        $commande = $em->getRepository('WebBundle:Commandes')->find($id);
        if ($statut==1){
            $commande->setDeliveryDate(new \DateTime());
        }
        elseif ($statut==2){
            $commande->setDateValidation(new \DateTime());
        }

        $commande->setStatus($statut);
        $em->merge($commande);
        $em->flush();
        return $this->redirectToRoute('commande_index');
    }
    /**
     * @Route("/Liste_des_commandes", name="ExportCommande" ,defaults={"_format"="xls","_filename"="Liste_des_commandes"}, requirements={"_format"="csv|xls|xlsx"})
     * @Template("@Backend/excel/CommandeExcel.xlsx.twig")
     */
    public function ExportModeleAction($_filename, Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $reference=$request->get('reference','');
        $statut=$request->get('statut','');
        $client=$request->get('client','');

        $commandes = $em->getRepository('WebBundle:Commande_Produit')->GetCommandeByPays($this->getuser()->getPays(),$reference,$statut,$client);
        return $this->render('@Backend/excel/CommandeExcel.xlsx.twig', array(
            'Modeles' => $commandes,
        ));
    }
    /**
     * @Route("/Details_commandes/{_filename}/{id}", name="ExportDetailsCommande" ,defaults={"_format"="xls","_filename"="Details_commandes"}, requirements={"_format"="csv|xls|xlsx"})
     * @Template("@Backend/excel/DetailsCommandeExcel.xlsx.twig")
     */
    public function ExportDetailsCommandeAction($_filename,$id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $commande = $em->getRepository('WebBundle:Commandes')->find($id);
        $produits= $em->getRepository('WebBundle:Commande_Produit')->GetProduitByCommande($commande);

        return $this->render('@Backend/excel/DetailsCommandeExcel.xlsx.twig', array(
            'commande' => $commande,
            'produits' => $produits,

        ));
    }
}
