<?php

namespace BackendBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use WebBundle\Entity\Categories;
use WebBundle\Entity\Pays;
use WebBundle\Entity\SousCategories;
use WebBundle\Entity\Station;

class SousCategorieType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom')
            ->add('categorie',EntityType::class, array(
                'required'   => true,
                'placeholder' => 'Choisir une categorie',
                'class' => 'WebBundle\Entity\Categories',
                'choice_label' => 'getNom',
                'query_builder' => function (EntityRepository $er )use($options){
                    return $er->createQueryBuilder('c')
                        ->leftJoin("c.pays", "p")
                        ->where('p = :pays ')
                        ->andWhere('c.isActive = true ')
                        ->setParameter('pays',$options['pays']);
                }
            ))
            ->add('imagefile', FileType::class, [
                'mapped' => false,
                'required' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SousCategories::class,
            'pays' => Pays::class,

        ]);
    }
}
