<?php

namespace WebBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OrderBy;

/**
 * @ORM\Entity(repositoryClass="WebBundle\Repository\PaysRepository")
 */
class Pays
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=3)
     */
    private $code;

    /**
     * @ORM\OneToMany(targetEntity="Station", mappedBy="pays", orphanRemoval=true)
     * @OrderBy({"nom" = "ASC"})
     */
    private $stations;

    /**
     * @ORM\OneToMany(targetEntity="Produits", mappedBy="pays", orphanRemoval=true)
     */
    private $produits;

    /**
     * @ORM\OneToMany(targetEntity="Categories", mappedBy="pays", orphanRemoval=true)
     */
    private $categories;
    /**
     * @ORM\OneToMany(targetEntity="SousCategories", mappedBy="pays", orphanRemoval=true)
     */
    private $sous_categories;
    /**
     * @ORM\OneToMany(targetEntity="Commandes", mappedBy="pays", orphanRemoval=true)
     */
    private $commandes;
    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @ORM\OneToOne(targetEntity="Utilisateurs", mappedBy="pays", cascade={"persist", "remove"})
     */
    private $user;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isActive = true;


    public function __construct()
    {
        $this->stations = new ArrayCollection();
        $this->categories = new ArrayCollection();
        $this->produits = new ArrayCollection();
        $this->sous_categories = new ArrayCollection();
        $this->commandes = new ArrayCollection();

        $time = new \DateTime();
        $time->setTimezone(new \DateTimeZone('Africa/Tunis'));
        $this->createdAt = $time;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param mixed $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param mixed $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return mixed
     */
    public function getStations()
    {
        return $this->stations;
    }

    public function addStation(Station $station): self
    {
        if (!$this->stations->contains($station)) {
            $this->stations[] = $station;
            $station->setPays($this);
        }

        return $this;
    }

    public function removeStation(Station $station): self
    {
        if ($this->stations->contains($station)) {
            $this->stations->removeElement($station);
            // set the owning side to null (unless already changed)
            if ($station->getPays() === $this) {
                $station->setPays(null);
            }
        }

        return $this;
    }


    /**
     * @return mixed
     */
    public function getProduits()
    {
        return $this->produits;
    }

    public function addProduit(Produits $produit)
    {
        if (!$this->produits->contains($produit)) {
            $this->produits[] = $produit;
            $produit->setPays($this);
        }

        return $this;
    }

    public function removeProduit(Produits $produit)
    {
        if ($this->produits->contains($produit)) {
            $this->produits->removeElement($produit);
            // set the owning side to null (unless already changed)
            if ($produit->getPays() === $this) {
                $produit->setPays(null);
            }
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCategories()
    {
        return $this->categories;
    }

    public function addCategorie(Categories $categorie)
    {
        if (!$this->categories->contains($categorie)) {
            $this->categories[] = $categorie;
            $categorie->setPays($this);
        }

        return $this;
    }

    public function removeCategorie(Categories $categorie)
    {
        if ($this->categories->contains($categorie)) {
            $this->categories->removeElement($categorie);
            // set the owning side to null (unless already changed)
            if ($categorie->getPays() === $this) {
                $categorie->setPays(null);
            }
        }
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSousCategories()
    {
        return $this->sous_categories;
    }

    public function addSousCategorie(SousCategories $sousCategorie)
    {
        if (!$this->sous_categories->contains($sousCategorie)) {
            $this->sous_categories[] = $sousCategorie;
            $sousCategorie->setPays($this);
        }

        return $this;
    }

    public function removeSousCategorie(SousCategories $sousCategorie)
    {
        if ($this->sous_categories->contains($sousCategorie)) {
            $this->sous_categories->removeElement($sousCategorie);
            // set the owning side to null (unless already changed)
            if ($sousCategorie->getPays() === $this) {
                $sousCategorie->setPays(null);
            }
        }
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCommandes()
    {
        return $this->commandes;
    }

    public function addCommande(Commandes $commande)
    {
        if (!$this->commandes->contains($commande)) {
            $this->commandes[] = $commande;
            $commande->setPays($this);
        }

        return $this;
    }

    public function removeCommande(SousCategories $commande)
    {
        if ($this->commandes->contains($commande)) {
            $this->commandes->removeElement($commande);
            // set the owning side to null (unless already changed)
            if ($commande->getPays() === $this) {
                $commande->setPays(null);
            }
        }
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param mixed $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser(Utilisateurs $user)
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * @param mixed $isActive
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
    }





}
