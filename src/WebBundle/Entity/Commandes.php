<?php

namespace WebBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Commandes
 *
 * @ORM\Table("commandes")
 * @ORM\Entity(repositoryClass="WebBundle\Repository\CommandesRepository")
 */
class Commandes
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="WebBundle\Entity\Utilisateurs", inversedBy="commandes")
     * @ORM\JoinColumn(nullable=false)
     */

    private $utilisateur;

    /**
     * @var boolean
     *
     * @ORM\Column(name="valider", type="boolean")
     */
    private $valider;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $created_at;

    /**
     * @var integer
     *
     * @ORM\Column(name="reference", type="integer")
     */
    private $reference;

    /**
     * @ORM\ManyToOne(targetEntity="Pays", inversedBy="commandes")
     * @ORM\JoinColumn(nullable=true)
     */
    private $pays;
    /**
     * @ORM\ManyToOne(targetEntity="Station", inversedBy="commandes")
     * @ORM\JoinColumn(nullable=true)
     */
    private $station;

    /**
     * @var integer
     *
     * @ORM\Column(name="date_reservation", type="datetime")
     */
    private $date_reservation;
    /**
     * @var integer
     *
     * @ORM\Column(name="date_reception", type="datetime")
     */
    private $date_reception;

    /**
     * @var string
     *
     * @ORM\Column(name="periode_reservation", type="string")
     */
    private $periode_reservation = 1;


    /**
     * @var string
     *
     * @ORM\Column(name="status", type="integer")
     */
    private $status = 0;

    /**
     * @ORM\Column(type="datetime",nullable=true)
     */
    private $delivery_date;

    /**
     * @ORM\Column(type="datetime",nullable=true)
     */
    private $date_validation;


    private $status_to_string = array(
        0 => "En cours de validation",
        1 => "En cours de livraison",
        2 => "Validée",
        3 => "Modifier",
        4 => "Annuler",
    );

    private $status_css_class= array(
        0 => "label-pending",
        1 => "label-primary",
        2 => "label-success",
        3 => "label-default",
        4 => "label-danger",
    );





    public  function getStatusToString()
    {
        return $this->status_to_string[$this->status];
    }


    public  function getStatusCssClass()
    {
        return $this->status_css_class[$this->status];
    }
    /**
     * Commandes constructor.
     * @param \DateTime $created_at
     */
    public function __construct()
    {
        $time = new \DateTime();
        $time->setTimezone(new \DateTimeZone('Africa/Tunis'));
        $this->created_at = $time;
    }



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set valider
     *
     * @param boolean $valider
     *
     * @return Commandes
     */
    public function setValider($valider)
    {
        $this->valider = $valider;

        return $this;
    }

    /**
     * Get valider
     *
     * @return boolean
     */
    public function getValider()
    {
        return $this->valider;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Commandes
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set reference
     *
     * @param integer $reference
     *
     * @return Commandes
     */
    public function setReference($reference)
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * Get reference
     *
     * @return integer
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * @return mixed
     */
    public function getPays()
    {
        return $this->pays;
    }

    /**
     * @param mixed $pays
     */
    public function setPays($pays)
    {
        $this->pays = $pays;
    }

    /**
     * @return mixed
     */
    public function getStation()
    {
        return $this->station;
    }

    /**
     * @param mixed $station
     */
    public function setStation(Station $station)
    {
        $this->station = $station;
    }

    /**
     * Set commande
     *
     * @param array $commande
     *
     * @return Commandes
     */
    public function setCommande($commande)
    {
        $this->commande = $commande;

        return $this;
    }

    /**
     * Get commande
     *
     * @return array
     */
    public function getCommande()
    {
        return $this->commande;
    }

    /**
     * Set dateReservation
     *
     * @param \DateTime $dateReservation
     *
     * @return Commandes
     */
    public function setDateReservation($dateReservation)
    {
        $this->date_reservation = $dateReservation;

        return $this;
    }

    /**
     * Get dateReservation
     *
     * @return \DateTime
     */
    public function getDateReservation()
    {
        return $this->date_reservation;
    }

    /**
     * Set periodeReservation
     *
     * @param string $periodeReservation
     *
     * @return Commandes
     */
    public function setPeriodeReservation($periodeReservation)
    {
        $this->periode_reservation = $periodeReservation;

        return $this;
    }

    /**
     * Get periodeReservation
     *
     * @return string
     */
    public function getPeriodeReservation()
    {
        return $this->periode_reservation;
    }

    /**
     * Set utilisateur
     *
     * @param \WebBundle\Entity\Utilisateurs $utilisateur
     *
     * @return Commandes
     */
    public function setUtilisateur(\WebBundle\Entity\Utilisateurs $utilisateur = null)
    {
        $this->utilisateur = $utilisateur;

        return $this;
    }

    /**
     * Get utilisateur
     *
     * @return \WebBundle\Entity\Utilisateurs
     */
    public function getUtilisateur()
    {
        return $this->utilisateur;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return Commandes
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set deliveryDate
     *
     * @param \DateTime $deliveryDate
     *
     * @return Commandes
     */
    public function setDeliveryDate($deliveryDate)
    {
        $this->delivery_date = $deliveryDate;

        return $this;
    }

    /**
     * Get deliveryDate
     *
     * @return \DateTime
     */
    public function getDeliveryDate()
    {
        return $this->delivery_date;
    }




    /**
     * Get grossiste
     *
     * @return string
     */
    public function getDateclient()
    {
        switch ($this->periode_reservation)
        {
            case 1: $periode = 'Matin de 8h - 12h'; break;
            case 2: $periode = 'Aprés midi de 13h - 17h'; break;
            case 3: $periode = 'Soir de 17h - 21h'; break;
        }
        return $this->date_reservation->format('Y-m-d')." le ".$periode;
    }

    /**
     * Set dateValidation
     *
     * @param \DateTime $dateValidation
     *
     * @return Commandes
     */
    public function setDateValidation($dateValidation)
    {
        $this->date_validation = $dateValidation;

        return $this;
    }

    /**
     * Get dateValidation
     *
     * @return \DateTime
     */
    public function getDateValidation()
    {
        return $this->date_validation;
    }

    /**
     * @return int
     */
    public function getDateReception()
    {
        return $this->date_reception;
    }

    /**
     * @param int $date_reception
     */
    public function setDateReception($date_reception)
    {
        $this->date_reception = $date_reception;
    }


}
