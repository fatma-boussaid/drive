<?php

namespace WebBundle\Repository;

/**
 * CategoriesRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class SousCategoriesRepository extends \Doctrine\ORM\EntityRepository
{
    public function getSousCategorieByPays($pays,$search,$categorie_id)
    {
        $q=$this->createQueryBuilder('sc')
            ->select('sc,c')
            ->leftJoin("sc.pays", "p")
            ->leftJoin("sc.categorie", "c")
            ->where('sc.pays = :pays')
            ->andWhere('sc.isActive = true')
            ->setParameter('pays', $pays);
        if($search !=""){
            $q  ->andWhere('sc.nom like :search')
                ->setParameter('search', '%'.$search.'%');

        }
        if($categorie_id !=""){
            $q  ->andWhere('c.id = :categorie_id')
                ->setParameter('categorie_id', $categorie_id);

        }
        return $q->getQuery()->getArrayResult();

    }
}
